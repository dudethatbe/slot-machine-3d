'use-strict';

const cellWidth = 210; //px
let cellHeight = 120; //px
let cells; // array of JSON objects [{name: "lemon", emoji: "🍋"}, ...]
let theta = 0; //(360deg - totalFruits)
let rotateXRegex = /rotateX\((.+)deg\)/;
let rotateYRegex = /rotateY\((.+)deg\)/;
let translateZRegex = /translateZ\((.+)px\)/;
let lastIndices = [0, 0, 0]; // keep track of index last used for each carousel
let firstLoad = true;

fetchCells();

let debugCheckbox = document.querySelector('#debug-checkbox');
debugCheckbox.checked = false;
debugCheckbox.addEventListener('change', toggleDebugContainers);

document.querySelector('#reset-button').addEventListener('click', () => {
  document.querySelector('#game-round').value = 0;
  document.querySelector('#total-money').value = 500;
  document.querySelector('#bet-money').value = 10;
});

let cellInput = document.querySelector('#cell-count');
cellInput.addEventListener('change', updateTheta);
cellInput.value = 15;

let spinTimer = document.querySelector('.spin-timer');
spinTimer.addEventListener('change', (e) => {
  let text = document.querySelector('#spin-label');
  let value = new Number(e.target.value);
  text.innerHTML = value.toFixed(1);
});
spinTimer.value = 2;

document.querySelector('#select-font').addEventListener('change', (e) => {
  document.querySelectorAll('.body-font').forEach(element => {
    element.style.fontFamily = e.target.value;
  });
});

document.querySelector('#test-win-round').addEventListener('click', (e) => {
  animateBell(true);
});

document.querySelector('#test-lose-round').addEventListener('click', (e) => {
  animateBell(false);
});

document.querySelector('.spin-button').addEventListener('click', spinCarousels);

/**
 * @description Animates bell and animated-border elements to use keyframes animations
 * @param {*} roundWasWon Sets animation type: `round-lost/won` `flash/dim-lights`
 */
function animateBell(roundWasWon) {
  let bell = document.querySelector('#bell');
  bell.classList.remove('round-lost');
  bell.classList.remove('round-won');
  // https://css-tricks.com/restart-css-animation/
  void bell.offsetWidth;
  bell.classList.add(roundWasWon ? 'round-won' : 'round-lost');

  let elements = document.querySelectorAll('.animated-border');
  elements.forEach((e) => {
    e.classList.remove('dim-lights');
    e.classList.remove('flash-lights');
    void e.offsetWidth;
    e.classList.add(roundWasWon ? 'flash-lights' : 'dim-lights');
  });
}

/**
 * @description Show or hide the debug-container element
 * @param {*} e
 */
function toggleDebugContainers(e) {
  let debugContainer = document.querySelector('#debug-container');
  debugContainer.classList.toggle('hidden');
}

/**
 * @description Event Listener for `cell-input` "change" event; Determines if it can create a carousel with the number of selected cells or not
 */
function updateTheta() {
  let value = parseInt(document.querySelector('#cell-count').value);
  document.querySelector('#cell-input-label').innerText = value;
  let chances = document.querySelector('#chances');
  if ((360 % value) !== 0) {
    document.querySelector('.spin-button').disabled = true;
    let box = document.querySelector('#message-box');
    box.innerText = `unable to display ${value} cells, try a different number`;
    box.style.fontSize = '35px';
    chances.innerText = 'N/A';
    document.querySelector('.theta').innerText = (360 / value).toFixed(2);
  } else {
    document.querySelector('.spin-button').disabled = false;
    let denominator = (Math.pow(value, 3)) / value;
    chances.innerText = `1/${denominator}`;
    addCells();
  }
}

/**
 * @description Make an AJAX request to the `cells.json` file
 */
function fetchCells() {
  let request = new XMLHttpRequest();
  request.open('GET', 'cells.json');
  request.addEventListener('load', addCells);
  request.addEventListener('error', error => {
    throw error;
  });
  request.send();
}

/**
 * @description Sets the cells of the tables to the `cells` object
 * @param {*} total The total number of cells that are available in the carousels
 * @param {*} firstLoad If true, the rows for the "tables" are created and appended
 */
function updateTable(total, firstLoad) {
  let tables = document.querySelectorAll('.grid-cells');
  tables.forEach(table => {
    let whichTable;
    if (table.parentElement.classList.contains('left-grid')) {
      whichTable = '.left-grid';
    } else if (table.parentElement.classList.contains('right-grid')) {
      whichTable = '.right-grid';
    }
    cells.forEach((cell, i) => {
      let cellName, cellValue;
      let opacity = i < total ? 1 : .2;
      let secondSet = i > 8;
      if (firstLoad) {
        cellName = document.createElement('div');
        cellName.className = 'grid-name-1';
        if (secondSet) {
          cellName.className = cellName.className.replace(/-1/, '-2');
          cellName.style.gridRow = i - 7;
        }
        cellName.innerText = cell.emoji;
        cellValue = document.createElement('div');
        cellValue.className = 'grid-value-1';
        if (secondSet) {
          cellValue.className = cellValue.className.replace(/-1/, '-2');
          cellValue.style.gridRow = i - 7;
        }
        cellValue.innerText = cell.value + 'X';
        cellName.setAttribute('data-cell-index', i);
        cellValue.setAttribute('data-cell-index', i);
        table.append(cellName);
        table.append(cellValue);
      } else {
        let cellNameQuery = `${whichTable} .grid-name${secondSet ? '-2' : '-1'}[data-cell-index="${i}"]`;
        let cellValueQuery = `${whichTable} .grid-value${secondSet ? '-2' : '-1'}[data-cell-index="${i}"]`;
        cellName = document.querySelector(cellNameQuery);
        cellValue = document.querySelector(cellValueQuery);
      }
      cellValue.style.opacity = opacity;
      cellName.style.opacity = opacity;
    });
  });
}

/**
 * @description Parses AJAX response, updates tables next to carousels, and updates cells in carousels
 */
function addCells() {
  if (this.response && !cells) {
    cells = JSON.parse(this.response);
  }
  lastIndices = [0, 0, 0];
  let totalCells = parseInt(document.querySelector('#cell-count').value);
  theta = Math.ceil(360 / totalCells);
  document.querySelector('.theta').innerText = theta;
  // determine length from theta to opposite line (tan)
  let radius = Math.ceil(
    (cellHeight / 2) /
    Math.tan(Math.PI / totalCells)
  );
  updateTable(totalCells, firstLoad);
  // get carousels as they appear in the DOM
  getCarousels(false).forEach(carousel => {
    carousel.style.transitionDuration = '0s';
    let carouselId = getCarouselId(carousel);
    for (let i = 0; i < cells.length; i++) {
      let cellDiv;
      if (firstLoad) {
        cellDiv = document.createElement('div');
        cellDiv.className = 'carousel_cell';
        cellDiv.innerHTML = cells[i].emoji;
        carousel.append(cellDiv);
        cells[i].index = i;
      } else {
        cellDiv = carousel.children[i];
      }
      if (i < totalCells) {
        cellDiv.style.opacity = 1;
        cellDiv.style.transform = `rotateX(${theta * i}deg) translateZ(${radius}px)`;
      } else {
        cellDiv.style.opacity = 0;
        cellDiv.style.transform = `none`;
      } // if first-load and last cell and last carousel
      if (firstLoad && ((i === (cells.length - 1)) && (carouselId === 1)))
        firstLoad = false;
    }
    carousel.style.transform = `translateZ(-${radius}px) rotateX(0deg)`;
  });
  let box = document.querySelector('#message-box');
  box.style.fontSize = '50px';
  box.innerText = 'Press "Spin" to start!';
}

/**
 * @description Triggers sequence of carousels to be spun in slot machine
 */
function spinCarousels() {
  let box = document.querySelector('#message-box');
  box.style.fontSize = '45px';
  box.innerText = 'spinning ...';
  let delay = 50;
  document.querySelector('.spin-button').disabled = true;
  let cellInput = document.querySelector('#cell-count');
  cellInput.disabled = true;
  let roundInput = document.querySelector('#game-round');
  let round = parseInt(roundInput.value);
  round++;
  roundInput.value = round;
  // get carousels sorted by data-carousel-id
  getCarousels(true).forEach(carousel => {
    setTimeout(spinCarousel, delay, carousel, cellInput.value);
    delay += 250;
  });
}

/**
 * @description Picks a random cell to land on, stores it, and rotates the carousel to that space
 * @param {*} carousel A carousel element from the DOM
 * @param {*} upperBound The maximum number of cells to choose from
 */
function spinCarousel(carousel, upperBound) {
  let spinTime = document.querySelector('.spin-timer').value;
  carousel.style.transitionDuration = spinTime + 's';
  let lastIndexI = carousel.getAttribute('data-carousel-id');
  let lastIndex = lastIndices[lastIndexI];
  let guess = 0;
  let randomIndex = getRandom(upperBound);
  if (lastIndex + randomIndex >= upperBound) {
    let spaceLeft = (upperBound - lastIndex);
    guess = randomIndex - spaceLeft;
  } else {
    guess = lastIndex + randomIndex;
  }
  lastIndices[lastIndexI] = guess;
  // log object carousel will land on and it's index
  // console.log(cells[lastIndices[lastIndexI]], carousel.getAttribute('data-carousel-id'));
  let selectedAngle = (randomIndex + (upperBound * 2)) * theta;
  let origin = getRotateX(carousel.style.transform);
  let destination = origin + selectedAngle;
  let radius = getTranslateZ(carousel.style.transform);
  let transformStr = `translateZ(-${radius}px) rotateX(-${destination}deg)`;
  carousel.style.transform = transformStr;
  setTimeout(updatePositions, spinTime * 1000, carousel);
}

/**
 * @description Determines whether the round is over or not. Update winnings and animate objects
 * @param {*} carousel Checks if carousel element is the `last carousel`
 */
function updatePositions(carousel) {
  let lastCarousel = 2;
  if (parseInt(carousel.getAttribute('data-carousel-id')) === lastCarousel) {
    let box = document.querySelector('#message-box');
    let amountBet = parseInt(document.querySelector('#bet-money').value);
    let totalInput = document.querySelector('#total-money');
    let totalAmount = parseInt(totalInput.value);
    let first = lastIndices[0];
    if (lastIndices.every((value) => {
      return value === first;
    })) {
      box.style.fontSize = '85px';
      box.innerText = 'wow !!!';
      let selectedCell = cells[lastIndices[0]];
      totalAmount += selectedCell.value * amountBet;
      animateBell(true);
    } else {
      box.style.fontSize = '35px';
      box.innerText = 'too bad ...'
      totalAmount -= amountBet;
      animateBell(false);
    }
    totalInput.value = totalAmount;
    document.querySelector('.spin-button').disabled = false;
    document.querySelector('#cell-count').disabled = false;
  }
}

/**
 * @description Get each .carousel element from DOM
 * @param {*} sort If true, the elements sorted by `data-carousel-id`, if false, then sorted by appearnce in DOM
 */
function getCarousels(sort) {
  let carousels = document.querySelectorAll('.carousel');
  if (sort) {
    return Array.from(carousels).sort(function (a, b) {
      let carA = getCarouselId(a);
      let carB = getCarouselId(b);
      if (carA > carB) {
        return 1;
      }
      if (carA < carB) {
        return -1;
      }
      return 0;
    });
  } else {
    return carousels;
  }
}

/**
 * @description Get a carousel's `data-carousel-id` integer value
 * @param {*} carousel
 */
function getCarouselId(carousel) {
  return parseInt(carousel.getAttribute('data-carousel-id'));
}

/**
 * @description Get a random number between 0 and the limit
 * @param {*} limit The upper limit of the random number to choose
 */
function getRandom(limit) {
  return Math.floor(Math.random() * limit) + 1;
}

/**
 * @description Get the current px from a `rotateX(0deg)` string
 * @param {*} transform The .style.transform property of an element
 */
function getRotateX(transform) {
  return testTransform(transform, rotateXRegex);
}

/**
 * @description Get the current deg from a `rotateY(0deg)` string
 * @param {*} transform The .style.transform property of an element
 */
function getRotateY(transform) {
  return testTransform(transform, rotateYRegex);
}

/**
 * @description Get the current px from a `translateZ(0px)` string
 * @param {*} transform The .style.transform property of an element
 */
function getTranslateZ(transform) {
  return testTransform(transform, translateZRegex);
}

/**
 * @description Captures integer value in elements style
 * @param {*} transform The .style.transform property of an element
 * @param {*} regex The regular expression to capture the integer value
 * @returns absolute integer value (px/deg) from transform string
 */
function testTransform(transform, regex) {
  if (regex.test(transform)) {
    return Math.abs(parseInt(regex.exec(transform)[1]));
  } else {
    return 0;
  }
}