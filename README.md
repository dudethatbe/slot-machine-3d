# slot-machine-3d
🎰 ring-a-ding-ding 🎰

lightweight, configurable, javascript and css slot machine

# preview
Checkout a live preview on Glitch 🐟 https://glitch.com/~slotmachine3d

# requirements

* Node.Js 18+
* Any browser that's relatively modern (emojis and 3D CSS)

# start
```
$ npm i
$ npm start
> slot-machine-3d@1.0.0 start
> node index.js

Your app is listening on port 12345
```

# history
This was my final project for a CS class that, in hindsight, I did not need to take. I had done so much frontend application work at this point, and the expectations were so low. I challenged myself to make something that any self-respecting shovelware artist would make: Yet Another Slot Machine. The real motivation was to make something animated in 3D using CSS. The chances of any other student attmempting that seemed low so I wanted to show off. Anyways, I couldn't find any great examples to work with online, but I found a really great 3D carousel animation project (https://3dtransforms.desandro.com/carousel). Luckily I took all those math classes and figuring out how big to make a circle based on a variable amount of squares seemed _easy_ at the time. It could use some work, but I inted to keep the first release as an example of where it started off at. 